#!/usr/bin/env python
# -*- coding: utf-8 -*-
#--------------------------------------------------
#
# Copyright (c) 2018 TeamDrive Systems GmbH
#
# All rights reserved.
#
# -------------------------------------------------

import sys
import base64


try:
    import httplib
except ImportError:
    # python 3 compatibility.
    import http.client as httplib

try:
    import urllib2
except ImportError:
    # python 3 compatibility.
    import urllib.parse as urllib2

try:
    import urlparse
except ImportError:
    # python 3 compatibility.
    import urllib.parse as urlparse


try:
    import json
except ImportError:
    # python 2.5 compatibility. eg: sudo apt-get install python-simplejson
    import simplejson as json

# Useful for very coarse version differentiation.
PY2 = sys.version_info[0] == 2
PY3 = sys.version_info[0] == 3

if PY3:
    def getFirst(i):
        return next(i)
else:
    def getFirst(l):
        return l[0]

strBase = str if PY3 else basestring

class TeamDriveCallFailed (Exception):
    def __init__(self, name, result):
        self.__name = name
        self.__result = result

    def __str__(self):
        return "Call %s failed with %s" % (self.__name, repr(self.__result))

class TeamDriveException (TeamDriveCallFailed):
    """
    In case of an error, the TeamDrive API returns a specific JSON document. Thy python Module will parse this error.

    **Example using curl**::

        curl -s '127.0.0.1:45454/api/getSpaces'
        {
          "error": 50,
          "error_message": "CJsonApi::ApiError (Error_Missing_Session)",
          "result": false,
          "status_code": 500
        }

    **Example using the command line**::

        $ ./TeamDriveApi.py '127.0.0.1:45454' getSpaces
        Traceback (most recent call last):
          File "./TeamDriveApi.py", line 1178, in <module>
            main()
          File "./TeamDriveApi.py", line 1174, in main
            result = (getattr(TeamDriveApi, command)(api, *params))
          File "./TeamDriveApi.py", line 307, in getSpaces
            return self._call("getSpaces")
          File "./TeamDriveApi.py", line 204, in _call
            return self._call(name, params, method)
          File "./TeamDriveApi.py", line 205, in _call
            raise TeamDriveException(name, jsonRes)
        __main__.TeamDriveException: Call getSpaces failed with {u'status_code': 500, u'error_message': u'CJsonApi::ApiError (Error_Missing_Session)', u'result': False, u'error': 50}


    """

    Error_Internal_Error = 1
    """
    """  # pylint: disable=W0105
    Error_HTTP_Method_Not_Allowed = 2
    """
    """  # pylint: disable=W0105
    Error_Unknown_Error = 3
    """
    """  # pylint: disable=W0105

    Error_Missing_Parameter = 20
    """
    """  # pylint: disable=W0105
    Error_Invalid_Parameter = 21
    """
    """  # pylint: disable=W0105

    Error_Insufficient_Rights = 30
    """
    """  # pylint: disable=W0105

    Error_Unknown_Space = 40
    """
    """  # pylint: disable=W0105
    Error_Unknown_User = 41
    """
    """  # pylint: disable=W0105
    Error_Unknown_AddressId = 42
    """
    """  # pylint: disable=W0105
    Error_Invalid_File_Key = 43
    """
    """  # pylint: disable=W0105

    Error_Missing_Session = 50
    """
    No or invalid credentials supplied.

    .. seealso:: Chapter :ref:`authentication-label`.
    """  # pylint: disable=W0105
    Error_Wrong_Login = 51
    """
    """  # pylint: disable=W0105
    
    Error_Feature_Upgrade = 70
    """
    The API call would force other members of the Space to upgrade their TeamDrive version, a confirmation step is required via ``setFeatureChoice``.
    
    In addition to the usual fields, the returned object will include the following::
    
        {
          ...
          "featureId": 10,
          "featureSpaceId": 123,
          "featureQuestion": "Using XYZ new feature will require other Space members to upgrade their installation, continue?",
          "featureHasOldBehaviour": True,
          ...
        }
    
    * ``featureId`` is the ID of the feature
    * ``spaceId`` is the ID of the Space that this feature-warning applies to (may be omitted for a feature that is not space-specific)
    * ``featureQuestion`` is a prompt that could be displayed to a user
    * ``featureHasOldBehaviour`` -> if ``True``, there is an older behaviour to fall back to, and calling ``setFeatureChoice`` with ``False`` will simply cause further API calls to use the old behaviour

    .. seealso:: :any:`setFeatureChoice`.
    
    .. note::
        
        This status is only returned if the setting ``http-api-enable-feature-alerts`` is ``True`` (the default is ``False``)
        
    """
    
    def __init__(self, name, result):
        super(TeamDriveException, self).__init__(name, result)

        self.__error = result["error"]
        """:type __error: int"""
        try:
            self.__error_string = filter(lambda e: e[1] == int(self.__error), TeamDriveException.__dict__.items())[0][0]
        except:
            self.__error_string = ""
        self.__error_message = result["error_message"]
        """:type __error_message: str"""
        self.__status_code = result["status_code"]
        """:type __status_code: int"""

    def getError(self):
        """
        The TeamDrive API Exception code
        :rtype: int
        """
        return self.__error

    def getErrorString(self):
        """
        A human readable version of this error
        :rtype: str
        """
        return self.__error_message

    def getStatusCode(self):
        """
        The HTTP status code of this exception.
        :rtype: int
        """
        return self.__status_code

class InternalTeamDriveApi:

    def __init__(self, server="[::1]:45454", username="", password=""):
        """
        :rtype: TeamDriveApi
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        """
        self._brokenAuthHeader = False
        if server:
            if "://" not in server:
                server = "http://" + server
            self._url = urlparse.urlparse(server)
            self._h = httplib.HTTPConnection(self._url.hostname, self._url.port)
            self.username = username
            self.password = password
            self._setCredentials()

    def _getAuthorizationHeader(self):
        if self._brokenAuthHeader is None or self._brokenAuthHeader is True:
            return {"Authorization": base64.b64encode((self.username + ":" + self.password).encode('utf8'))}
        else:
            return {"Authorization": b"Basic " + base64.b64encode((self.username + ":" + self.password).encode('utf8'))}

    def _setCredentials(self):
        if not self.username and not self._url.username:
            sys.stderr.write("Username?\n")
            self.username = sys.stdin.readline().strip()
        elif self._url.username:
            self.username = self._url.username.strip()

        if not self.password and not self._url.password:
            sys.stderr.write("Password?\n")
            self.password = sys.stdin.readline().strip()
        elif self._url.password:
            self.password = self._url.password.strip()

    def _call(self, name, params=None, method="GET", expectBinary=False):
        if params is None:
            params = {}
        if method != "GET" and isinstance(params, strBase):
            paramString = params
            call = u"/api/%s" % name
        else:
            paramString = u"&".join(map(lambda kv: kv[0] + u"=" + urllib2.quote(kv[1]), params.items()))
            if len(paramString) and method == "GET":
                call = u"/api/%s?%s" % (name, paramString)
            else:
                call = u"/api/%s" % name

        if call != u"/api/login":
            headers = self._getAuthorizationHeader()
        else:
            headers = {}
        self._h.request(method, call, body=(paramString if method != "GET" else None), headers=headers)
        res = self._h.getresponse()
        data = res.read()
        if expectBinary:
            return data

        jsonRes = json.loads(data.decode('utf-8'))
        if "error" in jsonRes:
            if self._brokenAuthHeader is False and (jsonRes["error"] == TeamDriveException.Error_Missing_Session or jsonRes["error"] == TeamDriveException.Error_Unknown_Error):
                self._brokenAuthHeader = None
                return self._call(name, params, method)
            raise TeamDriveException(name, jsonRes)
        if self._brokenAuthHeader is None:
            self._brokenAuthHeader = True
        return jsonRes

    def _checkedCall(self, name, params=None, method="GET"):
        if not params:
            params = {}
        ret = self._call(name, params, method)
        if "result" in ret and not ret["result"]:
            raise TeamDriveCallFailed(name, ret)
        return ret
    
    # shortcut for converting function arguments to a dict that omits 'self' and None, call like _mkParams(locals()) at the _start_ of the function
    def _mkParams(self, args):
        params = {}
        for k in args.keys():
            if k != 'self' and args[k] is not None:
                params[k] = args[k]
        return params

    def getSpaceIds(self):
        """
        Returns a list of all known Spaces Ids.

        **Example**::

            >>> api = TeamDriveApi()
            >>> api.getSpaceIds()
            [1,4,5,6]

        :rtype: list[int]
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._call("getSpaceIds")

    def getSpace(self, id):
        """
        Will return information about that Space.

        .. seealso:: :any:`getSpaceStatistics` for statistics.

        **Example using Python**::

            >>> api = TeamDriveApi()
            >>> map(api.getSpace, api.getSpaceIds())
            {.....}

        **Example using curl**::

            curl --user myTeamDriveUser http://127.0.0.1:45454/api/getSpace?id=310

                :returns: A single :ref:`space-json-data-type`.

        :param id: The Id of the Space
        :type id: int
        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._call("getSpace", {"id": str(id)})

    def getSpaceStatistics(self, id):
        """
        Returns Space statistics

        .. versionadded:: 4.1.1

        .. seealso:: :any:`getSpace`, :any:`getSpaces` for other information.

        :param id: The Id of the Space
        :type id: int
        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._call("getSpaceStatistics", {"id": str(id)})

    def getSpaces(self, sync = None):
        """
        Returns a list of all known Spaces.

        .. note::
        
            As this function was added with TeamDrive 4, the fall back for TeamDrive 3
            is to iterate over all Space Ids, therefore this function is much slower.

        .. seealso:: :any:`getSpaceStatistics` for statistics.

                :returns:

                        A list of :ref:`space-json-data-type` objects, for example

            .. code-block:: json

                [
                  {
                    "name": "My Space",
                    "creator": "$TMDR-1007",
                    "icon": "archived",
                    "status": "archived",
                    "status_message": "Space Archived",
                    "id": 1,
                    "spaceRoot": "/home/teamdrive/Spaces/My Space",
                    "time": "2015-09-15T11:17:48Z",
                    "permissionLevel": "read"
                  },
                  {
                    "name": "Api",
                    "creator": "...",
                    "spaceRoot": "/home/teamdrive/Spaces/Api",
                    "icon": "space",
                    "status": "active",
                    "status_message": "OK",
                    "id": 8,
                    "fileId": 1,
                    "time": "2015-09-15T11:17:48Z",
                    "permissionLevel": "readWrite"
                  }
                ]

        .. versionadded:: 4.0

        :param sync: (Optional, default ``False``) If true, and the key repository has not been fetched from the server yet, wait for the key repository before returning
        :type sync: bool | None

            .. versionadded:: 4.5.6

        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise ValueError: no JSON returned. Wrong URL?
        """

        params = {}

        if sync is not None:
            params["sync"] = sync

        try:
            return self._call("getSpaces", dict(params))
        except ValueError:
            return map(self.getSpace, self.getSpaceIds())

    def createSpace(
                    self,
                    spaceName,
                    disableFileSystem=None,
                    spacePath=None,
                    importExisting=None,
                    serverId=None,
                    inviteOwnDevices=None,
                    versionCount=None,
                    webAccess=None,
                    dataRetentionPeriod=None,
                    dataRetentionAutoTrash=None
                    ):
        """
        Creates a new Space with the given name.

        To convert an existing folder into a TeamDrive Space, you need to set the `importExisting`
        parameter to True.

        In case the url is called directly, the default for `disableFileSystem` is False and
        the `spacePath` is mandatory.
        
        .. _dataretention:
        
        .. admonition:: "Data Retention" Spaces
        
            Spefifying the ``dataRetentionPeriod`` parameter creates a special "Data Retention" Space. Any file in the Space will not be deletable by anyone except the
            Space Administrator until the file is older than the specified retention period. This can be used to satisfy legal requirements that documents be retained
            for a minimum length of time.

        Using this function requires the HTTP POST method.

        Throws :class:`TeamDriveException`, if the call fails.

                :returns: A dict containig the Id and a :ref:`space-json-data-type`.

        :param spaceName: The Name of the new Space
        :type spaceName: str
        :param disableFileSystem: This will disable the synchronization into the file system. TeamDrive will only synchronize meta-data if this is `True`.
        :type disableFileSystem: bool
        :param spacePath: File Path, in which the new Space will be created.
        :type spacePath: str
        :param importExisting: Tells the TeamDrive Agent to import an existing directory.
        :type importExisting: bool
        :param serverId: Hostserver to use
        :type serverId: int | None
        :param inviteOwnDevices: Whether to automatically invite your other teamdrive installations
        :type inviteOwnDevices: bool | None
        :param versionCount: Maximum number of versions of each file that will be kept
        :type versionCount: int | None
        :param webAccess: Whether this Space may be accessed from the web (one of "true", "false", or "default")
        
            .. versionadded:: 4.3.2
        
        :type webAccess: str | None
        :param dataRetentionPeriod: Minimum length of time (in months) before files can be deleted
        
            .. versionadded:: 4.6.3
        
        :type dataRetentionPeriod: int | None
        :param dataRetentionAutoTrash: Whether to automatically move files to the trash at the end of the data retention period
        
            .. versionadded:: 4.6.3
        
        :type dataRetentionAutoTrash: bool | None
        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise TeamDriveException: Space creation failed.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        
        return self._checkedCall("createSpace", self._mkParams(locals()), method="POST")

    def deleteSpace(self, id, delInFs, delOnServer, delInDB):
        """
        Deletes a Space.

        Using this function requires the HTTP POST method.

        Throws :class:`TeamDriveException`, if the call fails.

        :param id: The Id of the Space
        :type id: int
        :param delInFs: Delete the Space in the File System
        :type delInFs: bool
        :param delOnServer: Deletes the Space on the server. (requires Administrator rights)
        :type delOnServer: bool
        :param delInDB: Deletes the Space in the local Database
        :type delInDB: bool
        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise TeamDriveException: Deletion failed
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._checkedCall("deleteSpace", {"id": str(id), "delInFs": str(delInFs), "delOnServer": str(delOnServer), "delInDB": str(delInDB)}, method="POST")

    def joinSpace(self, id, disableFileSystem, spacePath=None, password=None, backupPath=None, fullRestore=None, snapshotId=None, sync=None):
        """
        Accepts an invitation, joins an archived Space or rejoins an active one.

        **Note: Password protected Invitations**:
            Invitations may be protected by a password and accepting these invitations require the password
            parameter.If an invitation is password protected, :any:`getSpace` adds :code:`"isPasswordProtected": true`
            to the reply.

        Using this function requires the HTTP POST method.

        Throws :class:`TeamDriveException`, if the call fails.

        :param id: The Id of the Space
        :type id: int
        :param disableFileSystem: This will disable the synchronization into the file system. TeamDrive will only synchronize meta-data if this is `True`.
        :type disableFileSystem: bool
        :param spacePath: Optional, Space root in the file system (must be absolute, to specify a foldername for the Space, use something like `/users/bob/MySpace`, otherwise use `/users/bob/`).
        :type spacePath: str | None
        :param password: Optional, Password for accepting this invitation.

            .. versionadded:: 4.1.1

        :type password: str | None
        :param backupPath: An optional backup path in case the space already exists in the file system.

            .. versionadded:: 4.2.1

        :type backupPath: str
        :param fullRestore: Do a full restore of the space.
        
            .. versionadded:: 4.2.1
        
        :type fullRestore: bool
        :param snapshotId: Do a preview-mode rollback of the Space to this snapshot (see :any:`commitCurrentSnapshot`)

            .. versionadded:: 4.3.2

        :type snapshotId: int
        :param sync: Only return once the Space is active (this can take some time) (default is ``False``)

            .. versionadded:: 4.5.6

        :type sync: bool
        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise TeamDriveException: Joining failed
        :raise ValueError: no JSON returned. Wrong URL?
        """
        params = {"id": str(id), "disableFileSystem": str(disableFileSystem)}
        if spacePath:
            params["spacePath"] = spacePath
        if password:
            params["password"] = password
        if backupPath:
            params["backupPath"] = backupPath
        if fullRestore:
            params["fullRestore"] = fullRestore
        if snapshotId:
            params["snapshotId"] = snapshotId
        if sync:
            params["sync"] = sync

        return self._checkedCall("joinSpace", dict(params), method="POST")

    def getSpacePermissionLevels(self):
        """
        Get a list of possible space permission "levels" (ie. read, read/write, admin, etc.) and their capabilities.
        Permission levels are returned in order of increasing "magnitude". The reply of this call is constant and won't change until the agent is restarted.

        :returns:

            .. code-block:: json

                [
                    {
                        "id": "none",
                        "name: "None",
                        "rights": {
                                "memberInvite": False,
                                "memberKick": False,
                                "memberSetRights": False,
                                "spaceRename": False,
                                "spaceDeleteOnServer": False,
                                "filePublish": False,
                                "fileDeleteOnServer": False,
                                "fileRename": False,
                                "fileRestoreFromTrash": False,
                                "fileChange": False
                                }
                    },
                    {
                        "id": "read",
                        "name": "Read"
                        "rights": { ... }
                    },
                    ...
                ]

        .. versionadded:: 4.1.1

        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._call("getSpacePermissionLevels")

    def inviteMember(self, spaceId, addressId, text, permissionLevel=None, password=None, name=None, sendEmail=True):
        """
                Invites a user into a Space. If no addressId is given, and 'name' is set to the email address of
                someone who doesn't have an account yet, they will get an email inviting them to join
                TeamDrive.

        Using this function requires the HTTP POST method.

        Throws :class:`TeamDriveException`, if the call fails.

        :param spaceId: The Id of the Space
        :type spaceId: int
        :param addressId: Already known Addressbook Id (optional if a name is provided)
        :type addressId: int | None
        :param text: Invitation Text
        :type text: str
        :param name: Username or an email address (required if no addressId is given)
        :type name: str | None
        :param permissionLevel: The new user permission Level. Optional.

            .. versionadded:: 4.1.1

        :type permissionLevel: str | None
        :param password: Optional. The password for the invitation.

            .. versionadded:: 4.1.1

        :type password: str | None
        :param sendEmail: Optional. If ``False``, don't email the invited user about the invitation (``True`` by default).

            .. versionadded:: 4.5.1

        :type sendEmail: bool | None
        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise TeamDriveException: Inviting that member failed
        :raise ValueError: no JSON returned. Wrong URL?
        """
        params = {"spaceId": str(spaceId), "addressId": str(addressId), "text": text}
        if permissionLevel:
            params["permissionLevel"] = permissionLevel
        if password:
            params["password"] = password
        if sendEmail:
            params["sendEmail"] = sendEmail
        return self._checkedCall("inviteMember", params, method="POST")

    def kickMember(self, spaceId, addressId):
        """
        Removes a member form this Space.

        Removing a member from a Space will not remove this user from :any:`getSpaceMemberIds`, but will set a
        this member to status "kicked" in :any:`getMember`.

        Using this function requires the HTTP POST method.

        Throws :class:`TeamDriveException`, if the call fails. For example, if the one doesn't
        have the rights to remove a user

        .. versionadded:: 4.0

        :param spaceId: The Id of the Space
        :type spaceId: int
        :param addressId: Already known Addressbook Id.
        :type addressId: int
        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise TeamDriveException: Removing this user failed
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._checkedCall("kickMember", {"spaceId": str(spaceId), "addressId": str(addressId)}, method="POST")

    def setMemberPermissionLevel(self, spaceId, addressId, permissionLevel):
        """
        Changes the permission level of another member.

        Using this function requires the HTTP POST method.

        Throws :class:`TeamDriveException`, if the call fails. For example, if the one doesn't
        have the rights to remove a user

        .. versionadded:: 4.1.1

        :param spaceId: The Id of the Space
        :type spaceId: int
        :param addressId: Already known Addressbook Id.
        :type addressId: int
        :param permissionLevel: the new permission level
        :type permissionLevel: str
        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise TeamDriveException: Removing this user failed
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._checkedCall("setMemberPermissionLevel", {"spaceId": str(spaceId), "addressId": str(addressId), "permissionLevel": permissionLevel}, method="POST")

    def getSpaceMemberIds(self, spaceId):
        """
        Returns all Addressbook Ids in a given Space.

        **Example**::

            $ python ./TeamDriveApi.py '127.0.0.1:45454' getSpaceMemberIds 3
            [1,2]

        .. versionadded:: 4.0

        .. deprecated:: 4.1.1
            Use :any:`getSpaceMembers` instead.

        :rtype: list[int]
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._call("getSpaceMemberIds", {"spaceId": str(spaceId)})

    def getSpaceMembers(self, spaceId):
        """
        Returns all Member infos in a given Space.

        .. versionadded:: 4.1

        :rtype: list[dict]
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._call("getSpaceMembers", {"spaceId": str(spaceId)})

    def getMember(self, spaceId, addressId):
        """
        Returns information about a member of a Space.

        **Example**::

            $ python ./TeamDriveApi.py '127.0.0.1:45454' getMember 3 1
            
        :returns:

            .. code-block:: json

                {
                    "status": "active",
                    "spaceId": 3,
                    "addressId": 1
                }

        :param spaceId: The Id of the Space
        :type spaceId: int
        :param addressId: Already known Addressbook Id.
        :type addressId: int
        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._call("getMember", {"spaceId": str(spaceId), "addressId": str(addressId)})

    def quit(self, logout):
        """
        Quits TeamDrive.

        Using this function requires the HTTP POST method.

        Throws :class:`TeamDriveException`, if the call fails.

        :param logout: Quit and also log out the current user. See :any:`login` to log in afterwards.
        :type logout: bool
        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._checkedCall("quitApplication", {"logout": str(logout)}, method="POST")

    def getLoginInformation(self):
        """
        Returns information about the current user.

        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._call("getLoginInformation")

    def about(self):
        """
        Returns information about TeamDrive.

        :returns:
        
            .. code-block:: json

                {
                  "version": "4.0.12 (Build: 1294)",
                }

        .. versionchanged:: 4.1.1 no longer returns license infos. call :any:`getLicense` instead.

        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._call("about")

    def getLicense(self):
        """
        Returns information about the current License.

        :returns:

            .. code-block:: json

                {
                  "distributor": "DOCU",
                  "licenseKey": "DOCU-8782-5485-7266",
                  "licenses": [
                    "WebDAV",
                    "Professional"
                  ]
                }

        .. versionadded:: 4.1.1

        .. seealso:: :any:`setLicenseKey` to set the license.

        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._call("getLicense")

    def login(self):
        """
        A TeamDrive Client installation is bound to a single TeamDrive account. Calling :any:`login` authenticates TeamDrive user and creates a new device.
        this is typically the first call to an Agent.

        Using this function requires the HTTP POST method.

        Throws :class:`TeamDriveException`, if the call fails.

        **Example as Command line**::

            ./TeamDriveApi.py '127.0.0.1:45454' --user myUser --pass myPass login

        **Example with Curl**:

            .. code-block:: bash

                curl 'https://127.0.0.1:45454/login' --data-binary '{"username":"My User","password":"My Password"}'

        **Example**::

            >>> api = TeamDriveApi("127.0.0.1:45454", "My Username", "My Password")
            >>> api.login()

        .. note:: On the desktop client, this call is not available, as the API will not be available until the user has successfully logged
            in. This is a technical limitation of the Gui Client.

        .. seealso:: Chapter :ref:`authentication-label` regarding Authentication.

        :param username: The Username
        :type username: str
        :param password: The Password
        :type password: str
        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise TeamDriveException: Logging failed.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._checkedCall("login", {"username": self.username, "password": self.password}, method="POST")

    def performExternalAuthentication(self, authToken):
        """
        Login using an external authentication token. The registration server must be configured to validate the
        token, refer the chapter "External Authentication" in the "TeamDrive Registration Server Reference Guide"
        for details.

        :param authToken: The token generated by the authentication service
        :type authToken: str
        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise TeamDriveException: Login failed.
        """
        return self._checkedCall("performExternalAuthentication", {"authToken": authToken}, method="POST")

    def requestResetPassword(self, username):
        """
        This call will reset the password of the user.

        .. versionadded:: 4.1.1

        :param username: The Username
        :type username: str
        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise TeamDriveException: Logging failed.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._checkedCall("requestResetPassword", {"username": self.username}, method="POST")

    def getFile(self, id):
        """
        Returns information about the given File Id.

                :returns: A :ref:`file-json-data-type` object

        :param id: The File id
        :type id: int
        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._call("getFile", {"id": str(id)})

    def getFiles(self, spaceId, filePath, trashed):
        """
        Returns a list of files matching Space Id, file path and trashed flag.

                :returns: A list of :ref:`file-json-data-type` objects

        :param spaceId: The Id of the Space
        :type spaceId: int
        :param filePath: the path of the file in the Space.
        :type filePath: str
        :param trashed: Is the file in the trash?
        :type trashed: bool
        :rtype: list[dict]
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._call("getFiles", {"spaceId": str(spaceId), "filePath": filePath, "trashed": str(trashed)})

    def getFolderContent(self, spaceId, filePath, trashed):
        """
        Returns a list of file Ids in a folder of a Space.

        **Example**:
        
            .. code-block:: bash

                python ./TeamDriveApi.py '127.0.0.1:45454' getFolderContent 3 / False
                [756,767,737,769,629,763,628,630,830,765]

        :param spaceId: The Id of the Space
        :type spaceId: int
        :param filePath: The parent folder path.
        :type filePath: str
        :param trashed: In Trash?
        :type trashed: bool
        :rtype: list[int]
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._call("getFolderContent", {"spaceId": str(spaceId), "filePath": filePath, "trashed": str(trashed)})

    def getFullFolderContent(self, spaceId, filePath, trashed):
        """
        Returns a list of file information in a folder of a Space.

        **Example**:

            .. code-block:: bash

                $ python ./TeamDriveApi.py '127.0.0.1:45454' getFullFolderContent 3 / False

            .. code-block:: json

                [
                  {
                    "isDir": true,
                    "name": "My folder",
                    "currentVersionId": 0,
                    "spaceId": 3,
                    "isInFileSystem": true,
                    "hasNameConflict": false,
                    "hasVersionConflict": false,
                    "path": "/",
                    "id": 756,
                    "permissions": "rwxr-xr-x"
                  },
                  {
                    "isDir": false,
                    "name": "My File",
                    "currentVersionId": 0,
                    "spaceId": 3,
                    "isInFileSystem": true,
                    "hasNameConflict": false,
                    "hasVersionConflict": false,
                    "path": "/",
                    "id": 767,
                    "permissions": "rwxr-xr-x"
                  }
                ]

        .. note::

            As this function was added with TeamDrive 4, the fall back for TeamDrive 3
            is to iterate over getFolderContent, therefore this function is much slower.

        .. seealso:: :any:`getFile` and :any:`getFolderContent`

        .. versionadded:: 4.0

        :returns: A list of :ref:`file-json-data-type` objects
        :param spaceId: The Id of the Space
        :type spaceId: int
        :param filePath: The parent folder path.
        :type filePath: str
        :param trashed: In Trash?
        :type trashed: bool
        :rtype: list[dict]
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        try:
            return self._call("getFullFolderContent", {u"spaceId": str(spaceId), u"filePath": filePath, u"trashed": str(trashed)})
        except ValueError:
            return map(self.getFile, self.getFolderContent(spaceId, filePath, trashed))

    def findFiles(self, spaceIds=None, path="/", search=None, recursive=False, filters=None, sortBy="name", start=0, limit=-1):
        """
        This function can be used to create a paged list of files in all Spaces, a single Space, or a single folder.

        The result is returned like this:

        .. code-block:: json

            {
                totalFiles: 123,
                files: [...]
            }

        ``totalFiles`` is the number of files that were matched, ``files`` is a subset of the matched files based on
        the ``start`` and ``limit`` parameters. See :any:`getFile` for the format of the individual file objects.

        The parameter ``filter`` is a comma-seperated list of strings indicating special filters. Valid values are:

            * ``conflicted``: Show only files that have name- or version conflicts
            * ``directory``: Show only folders
            * ``trashed``: Only files that are in the trash
            * ``invalidPath``: Only files that have an invalid path or name for the OS that the agent is running on
            * ``offlineAvailable``: Show only files that have been set to "offline-available"

        The above filters can also be prefixed with a "!" to invert them, eg. ``!trashed`` to show only files that are
        *not* in the trash

        Additionally, *one* of the following can be included in the filter list (these values can't be prefixed with "!"):

            * ``unconfirmed``: Show only files where the latest version has not been processed by TeamDrive yet
            * ``commented``: Show only files that have comments
            * ``published``: Show only published files

        The returned list is sorted according to the ``sortBy`` parameter, which contains a list of file properties that
        are considered in order (eg. specify ``[isDir, name]`` to sort by name, but list directories first). Valid
        values in the list are:

            * ``isDir``: List directories first
            * ``name``: Sort by file name
            * ``added``: sort by the time this file was added to the Space
            * ``created``: Sort by date of file creation
            * ``size``: Sort by size (directories considered as having a size of 0)
            * ``modified``: Sort by last-modified date (directories are sorted as though the date was 0)

        All of the sort-types are in ascending order by default, and can be inverted by prefixing them with "!", eg.
        ``!size`` to list in order of descending size.

        .. versionadded:: 4.5.1

        :param spaceIds: The IDs of the Spaces to search (default if not specified: search all Spaces)
        :type spaceIds: list[int]
        :param path: The path in the Space(s) under which to search for files. (Default is '/')
        :type path: str
        :param search: If set, files will be filtered by matching the file name against this parameter
        :type search: str
        :param recursive: Whether to recursively search subfolders of the given path (``False`` by default)
        :type recursive: bool
        :param filters: A comma-separated list of special filters. See the descriprion of this function for details
        :type filters: list[str]
        :param sortBy: A list of file properties to sort by, see the description of this function for details (default is ``name``)
        :type sortBy: list[str]
        :param start: The first row of the matched set of files to return (0-indexed, the default if not specified is ``0``)
        :type start: int
        :param limit: Maximum number of rows to return, or ``-1`` to return all rows (default is ``-1``)
        :type limit: int
        :return: A dict with the number of files matched, and the subset of those files specified by ``start`` and ``limit``
        :rtype: dict
        """

        return self._call("findFiles", {
            "spaceIds": spaceIds,
            "path": path,
            "search": search,
            "recursive": recursive,
            "filters": filters,
            "sortBy": sortBy,
            "start": start,
            "limit": limit
        })

    def getReadConfirmations(self, spaceId, addressId=0, fileId=0, filter="confirmed", onlyLatest=True, fileSearch="", limit=-1, start=0):
        """
        Fetch "Read Confirmations" (or missing Read Confirmations) for a Space, file, folder or Space member.

        The result is returned like this:

        .. code-block:: json

            {
                totalConfirmations: 123,
                confirmations: [
                    {
                        addressId : 1,
                        confirmed : true,  // would be False if this file version hadn't been confirmed by this user
                        fileId : 142950,
                        name : "new.txt",
                        path : "/",
                        time : 1498746732698.0,
                        totalConfirmations : 1,   // # of confirmations for this file, included if onlyLatest is True
                        versionId : 117039,
                        versionNo : 4
                    },
                    ...
                ]
            }

        .. versionadded:: 4.5.1

        :param spaceId:
        :type spaceId: int
        :param addressId: Only get Read Confirmations for this user (optional)
        :type addressId: int
        :param fileId: Only get Read Confirmations for this file (optional)
        :type fileId: int
        :param filter: One of ``confirmed``, ``unconfirmed`` or ``all`` to fetch Read Confirmations, missing cofirmations, or everything (defaults to ``confirmed`` if not set)
        :type filter: string
        :param onlyLatest: If ``False`` fetch every confirmation for every version of this file. Default is ``True``: fetch only the latest confirmation per user and file
        :type onlyLatest: bool
        :param fileSearch: Filter by file name based on this string
        :type fileSearch: string
        :param limit: Fetch at most this many confirmations (default is ``-1``, ie. no limit)
        :type limit: int
        :param start: Index from which to start returning results (default is 0, use together with ``limit`` to implement paging)
        :type start: int
        :return: A dict with the number of confirmations matched, and the subset of those items specified by ``start`` and ``limit``
        :rtype: dict
        """

        return self._call("getReadConfirmations", {
            "spaceId": spaceId,
            "addressId": addressId,
            "fileId": fileId,
            "filter": filter,
            "onlyLatest": onlyLatest,
            "fileSearch": fileSearch,
            "start": start,
            "limit": limit
        })

    def getFolderInfo(self, spaceId, filePath, trashed):
        """
        Returns information about the size of a folder:

        .. code-block:: json

            {
              "files": 10,
              "size": 3288549
            }

        .. versionadded:: 4.5.0

        :param spaceId: The Id of the Space
        :type spaceId: int
        :param filePath: The parent folder path.
        :type filePath: str
        :param trashed: In Trash?
        :type trashed: bool
        :rtype: list[int]
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._call("getFolderContent", {"spaceId": str(spaceId), "filePath": filePath, "trashed": str(trashed)})

    def createFolder(self, spaceId, filePath, trashed):
        """
        Creates a new folder in that Space

        **Example**:

            .. code-block:: bash

                $ ./TeamDriveApi.py '127.0.0.1:45454' createFolder 3 '/my new folder' False

            .. code-block:: json

                {
                  "result": true,
                  "file": {
                    "isDir": true,
                    "name": "my new folder",
                    "currentVersionId": 0,
                    "spaceId": 3,
                    "isInFileSystem": false,
                    "path": "/",
                    "id": 756,
                    "permissions": "rwxr-xr-x"
                  }
                }

        .. seealso:: :any:`getFullFolderContent` or :any:`getFolderContent` for getting the content of this folder

        .. versionadded:: 4.0

        :returns: A dict containing a :ref:`file-json-data-type` object
        :param spaceId: The Id of the Space
        :type spaceId: int
        :param filePath: The full folder path.
        :type filePath: str
        :param trashed: In Trash?
        :type trashed: bool
        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._call("createFolder", {"spaceId": str(spaceId), "filePath": filePath, "trashed": str(trashed)}, method="POST")

    def putFile(self, spaceId, path, data):
        """
        This is a Python specific accessor to the "files" API. The files API is another API endpoint to handle
        file content instead of meta data. Urls used by the files API are generated by concatenating
        the API endpoint :code:`files/` plus the local Space Id (eg. :code:`3`) plus the full file path relative
        to the Space root (eg. :code:`/my/path/to/the/file`). Such a url could look like this:
        :code:`http://127.0.0.1:45454/files/3/my/path/to/the/file`. This API does not support reading and writing files
        that are in the trash.

        This Uploads a file into a Space. The file creation is done asynchronously, a successful return code does not indicate
        a successful upload to the Server. Use :any:`getFiles` to request the current state of that file.

        **Example using telnet**::

            telnet 127.0.0.1 45454
            PUT /files/3/testfile HTTP/1.1
            Host: 127.0.0.1:45454
            Accept-Encoding: identity
            Content-Length: 4

            test

        **Example using curl**::

            echo "test" | curl -X PUT -d @- http://127.0.0.1:45454/files/3/testfile

        :returns:

            a dict containgng a :ref:`file-json-data-type` object

            .. code-block:: json

                {
                  "file": {
                    "confirmed": false,
                    "creator": "local",
                    "currentVersionId": 6156,
                    "hasComments": false,
                    "icon": "default",
                    "id": 7195,
                    "isDir": false,
                    "name": "testfile",
                    "path": "/",
                    "permissions": "rw-rw-r--",
                    "size": 4,
                    "spaceId": 3,
                    "time": "2015-10-22T14:09:45"
                  },
                  "result": true
                }

        .. note:: In TeamDrive 3, this API endpoint was called :code:`/webdav`.
        
        :param spaceId: The Id of the Space
        :type spaceId: int
        :param path: the path of the file in the Space.
        :type path: str
        :param data: The binary data
        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        """
        url = urllib2.quote("/webdav/" + str(spaceId) + ("" if path.startswith("/") else "/") + path)

        self._h.request("PUT", url, data, headers=self._getAuthorizationHeader())
        return self._h.getresponse().read()

    def _downloadFile(self, spaceId, path):
        """
        This is a Python specific accessor to the "files" API. The files API is another API endpoint to handle
        file content instead of meta data. Urls used by the files API are generated by concatenating
        the API endpoint :code:`files/` plus the local Space Id (eg. :code:`3`) plus the full file path relative
        to the Space root (eg. :code:`/my/path/to/the/file`). Such a url could look like this:
        :code:`http://127.0.0.1:45454/files/3/my/path/to/the/file`. This API does not support reading and writing files
        that are in the trash.

        Downloads a file from the Agent.

        .. note:: In TeamDrive 3, this API endpoint was called :code:`/webdav`.

        :param spaceId: The Id of the Space
        :type spaceId: int
        :param path: the path of the file in the Space.
        :type path: str
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        """
        url = urllib2.quote("/webdav/" + str(spaceId) + ("" if path.startswith("/") else "/") + path)

        self._h.request("GET", url, headers=self._getAuthorizationHeader())
        res = self._h.getresponse()
        return True if res.status == 200 else False, res

    def zipFolder(self, spaceId, path, recursive):
        """
        This is a Python specific accessor to the "zip" API which can be used to download a .zip archive of a folder.
        Because downloading, zipping, and returning the archive can take a long time depending on the size of the
        folder, this process is broken up into several steps:

            1. POST to :code:`<api endpoint>/zip[Recursive]/<space id><folder path>`
                This will start the process of downloading the neccessary files and zipping them. As indicated above,
                use :code:`.../zipRecursive/...` to include subfolders in the archive, or :code:`.../zip/...` to only
                include the top level

            2.  GET to :code:`<api endpoint>/zipProgress/<space id><folder path>`
                Use this request to poll the status of the task. Returns a json object like:

                .. code-block:: json

                    {
                        "totalFiles": 100,
                        "filesZipped": 50,
                        "status": "status text",
                        "isDone": false
                    }

            3. GET :code:`<api endpoint>/zip/<space id><folder path>`
                Downloads the finished archive

            4. DELETE :code:`<api endpoint>/zip/<space id><folder path>`
                This will clean up resources for this job on the server, and cancel the job if is still running.

        .. versionadded:: 4.5.0

        :param spaceId: The Id of the Space
        :type spaceId: int
        :param path: the path of the folder in the Space.
        :type path: str
        :param recursive: whether to recursively zip subfolders
        :type recursive: bool
        :rtype: dict
        """
        url = urllib2.quote(
            ("/zipRecursive/" if recursive else "/zip/")
            + str(spaceId)
            + ("" if path.startswith("/") else "/")
            + path
        )

        self._h.request("POST", url, headers=self._getAuthorizationHeader())
        return self._h.getresponse().read()

    def zipProgress(self, spaceId, path):
        """
        Get the status of a running zip-folder task, see :any:`zipFolder`

        .. note:: This is not an API function, just a python wrapper for the 'zip' endpoint, see :any:`zipFolder`

        .. versionadded:: 4.5.0

        :param spaceId: The Id of the Space
        :type spaceId: int
        :param path: the path of the folder in the Space.
        :type path: str
        :rtype: dict
        """
        url = urllib2.quote("/zipProgress/" + str(spaceId) + ("" if path.startswith("/") else "/") + path)

        self._h.request("GET", url, headers=self._getAuthorizationHeader())
        return self._h.getresponse().read()

    def zipDownload(self, spaceId, path):
        """
        Download the result archive of a zip-folder task, see :any:`zipFolder`

        .. note:: This is not an API function, just a python wrapper for the 'zip' endpoint, see :any:`zipFolder`

        .. versionadded:: 4.5.0

        :param spaceId: The Id of the Space
        :type spaceId: int
        :param path: the path of the folder in the Space.
        :type path: str
        """
        url = urllib2.quote("/zip/" + str(spaceId) + ("" if path.startswith("/") else "/") + path)

        self._h.request("GET", url, headers=self._getAuthorizationHeader())
        return self._h.getresponse()

    def zipCancel(self, spaceId, path):
        """
        Cancel/clean up a zip-folder task, see :any:`zipFolder`

        .. note:: This is not an API function, just a python wrapper for the 'zip' endpoint, see :any:`zipFolder`

        .. versionadded:: 4.5.0

        :param spaceId: The Id of the Space
        :type spaceId: int
        :param path: the path of the folder in the Space.
        :type path: str
        :rtype: dict
        """
        url = urllib2.quote("/zip/" + str(spaceId) + ("" if path.startswith("/") else "/") + path)

        self._h.request("DELETE", url, headers=self._getAuthorizationHeader())
        return self._h.getresponse().read()

    def moveFile(self, spaceId, *args):
        """
        Either `moveFile(self, spaceId, filePath, trashed, newFilePath)` or `moveFile(self, spaceId, fileId, newFilePath)`
        Moves a file from `filePath` or `fileId` to `newFilePath` in the given Space.

        Using this function requires the HTTP POST method.

        Throws :class:`TeamDriveException`, if the call fails.

        :param spaceId: The Id of the Space
        :type spaceId: int
        :param filePath: the path of the file in the Space.
        :type filePath: str
        :param trashed: Is the file in the trash?
        :type trashed: bool
        :param fileId: fileIf of the file meing moved
        :type trashed: int
        :param newFilePath: The destination file path.
        :type newFilePath: str
        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise TeamDriveException: Failed to queue command.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        if len(args) == 3:
            return self._checkedCall("moveFile", {"spaceId": str(spaceId), "filePath": args[0], "trashed": str(args[1]), "newFilePath": args[2]}, method="POST")
        elif len(args) == 2:
            return self._checkedCall("moveFile", {"spaceId": str(spaceId), "fileId": str(args[0]), "newFilePath": args[1]}, method="POST")

    def setTrashed(self, fileId, inTrash):
        """
        Moves a file into or out of the trash. Trashing a file also removes the file from the local file system.

        Throws :class:`TeamDriveException`, if the call fails.

        Call :any:`deleteFileFromTrash` to delete the file physically afterwards.

        .. seealso:: :any:`removeLocallyFile` and :any:`deleteFileFromTrash`.

        .. versionadded:: 4.0

        :param fileId: The Id of the Space
        :type fileId: int
        :param inTrash: Is the file in the trash?
        :type inTrash: bool
        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise TeamDriveException: Failed to queue command.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._checkedCall("setTrashed", {"fileId": str(fileId), "inTrash": str(inTrash)}, method="POST")

    def publishFile(self, fileId, password=None, expiryDate=None, createShortUrl=None, encrypted=None):
        """
        Publishes the recent version of that file. The parameters `password` and `expiryDate` are optional.
        Throws :class:`TeamDriveException`, if the call fails.

        .. versionadded:: 4.1

        :param fileId: The Id of the File
        :type fileId: int
        :param password: Is the file in the trash?
        :type password: str
        :param expiryDate: ISO 8601 extended format: either YYYY-MM-DD for dates or YYYY-MM-DDTHH:mm:ss, YYYY-MM-DDTHH:mm:ssTZD (e.g., 1997-07-16T19:20:30+01:00) for combined dates and times.
        :type expiryDate: str
        :param createShortUrl: Whether to create a shortened URL
        
            .. versionadded:: 4.6.3
        
        :type createShortUrl: bool | None
        :param encrypted: Enable server-side encryption of the published file
        
            .. versionadded:: 4.6.3
        
        :type encrypted: bool | None
        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise TeamDriveException: Failed to queue command.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        params = {"fileId": str(fileId)}
        if password:
            params["password"] = password
        if expiryDate:
            params["expiryDate"] = str(expiryDate)
        if createShortUrl:
            params["createShortUrl"] = str(createShortUrl)
        if encrypted:
            params["encrypted"] = str(encrypted)

        return self._checkedCall("publishFile", params, method="POST")

    def unpublishFile(self, fileId):
        """
        Unpublishes the file.
        Throws :class:`TeamDriveException`, if the call fails.

        .. versionadded:: 4.1

        :param fileId: The Id of the File
        :type fileId: int
        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise TeamDriveException: Failed to queue command.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._checkedCall("unpublishFile", {"fileId": str(fileId)}, method="POST")

    def lockFile(self, fileId):
        """
        Lock a file.
        Throws :class:`TeamDriveException`, if the call fails.

        .. versionadded:: 4.2.1

        :param fileId: The Id of the File
        :type fileId: int
        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise TeamDriveException: Failed to queue command.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._checkedCall("lockFile", {"fileId": str(fileId)}, method="POST")

    def unlockFile(self, fileId):
        """
        Lock a file.
        Throws :class:`TeamDriveException`, if the call fails.

        .. versionadded:: 4.2.1

        :param fileId: The Id of the File
        :type fileId: int
        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise TeamDriveException: Failed to queue command.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._checkedCall("unlockFile", {"fileId": str(fileId)}, method="POST")

    def deleteFileFromTrash(self, fileId):
        """
        Deletes a file from Trash by physically deleting the file from the server. This call fails, if you do not have the rights to delete files,
        it is a directory or the file is not in the trash.

        Using this function requires the HTTP POST method.

        **Example**::

            >>> api = TeamDriveApi()
            >>> for inTrash in api.getFullFolderContent(1, "/", True):
            >>>     api.deleteFileFromTrash(inTrash["id"])


        Throws :class:`TeamDriveException`, if the call fails.

        .. seealso:: :any:`removeLocallyFile` and :any:`setTrashed`.

        .. versionadded:: 4.0

        :param fileId: The Id of the Space
        :type fileId: int
        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise TeamDriveException: Failed to queue command.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._checkedCall("deleteFileFromTrash", {"fileId": str(fileId)}, method="POST")


    def removeLocallyFile(self, id, recursive=False):
        """
        Removes a file form the local file system by disabling the offline-available flag, but does not move this file
        into the trash or removes the file from the Space. This operation is not synchronized to other clients.

        A successful return code does not indicate a successful execution,
        because it is done asynchronously.

        Using this function requires the HTTP POST method.

        .. seealso:: :any:`deleteFileFromTrash` and :any:`setTrashed`.

        :param id: the file to be removed
        :type id: int
        :param recursive: Directories only: recursive remove
        :type recursive: bool
        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise TeamDriveException: Failed to queue command.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._checkedCall("removeLocallyFile", {"id": str(id), "recursive": str(recursive)}, method="POST")

    def restoreLocallyFile(self, id, recursive=False):
        """
        Restores a file form the local file system. A successful return code does not indicate a successful execution,
        because it is done asynchronously. This operation is not synchronized to other clients.

        Using this function requires the HTTP POST method.

        :param id: the file to be restored
        :type id: int
        :param recursive: Directories only: recursive restore
        :type recursive: bool
        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise TeamDriveException: Failed to queue command.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._checkedCall("restoreLocallyFile", {"id": str(id), "recursive": str(recursive)}, method="POST")

    def getVersion(self, versionId):
        """
        Returns a single version.

        >>> versionId = 42
        >>> TeamDriveApi().getVersion(versionId)["versionId"] == versionId

        .. versionadded:: 4.1.4

        :param versionId: the id of the version
        :type versionId: int
        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._call("getVersion", {"versionId": str(versionId)})

    def getVersions(self, fileId):
        """
        Returns all versions of a file.

        .. versionadded:: 4.1.4

        :rtype: list[dict]
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._call("getVersions", {"fileId": str(fileId)})

    def deleteVersion(self, versionId):
        """
        Deletes a file version finally from the server. This call fails, may may not have the rights to delete files,

        Using this function requires the HTTP POST method.

        Throws :class:`TeamDriveException`, if the call fails.

        .. seealso:: :any:`getVersion`

        .. versionadded:: 4.1.4

        :param versionId: The Id of the version
        :type versionId: int
        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise TeamDriveException: Failed to queue command.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._checkedCall("deleteVersion", {"versionId": str(versionId)}, method="POST")

    def setCurrentVersion(self, versionId):
        """
        Changes the current version of a file to this version.

        Using this function requires the HTTP POST method.

        Throws :class:`TeamDriveException`, if the call fails.

        .. seealso:: :any:`getVersion`

        .. versionadded:: 4.1.4

        :param versionId: The Id of the version
        :type versionId: int
        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise TeamDriveException: Failed to queue command.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._checkedCall("setCurrentVersion", {"versionId": str(versionId)}, method="POST")

    def resolveVersionConflict(self, versionId):
        """
        Changes the current version of a file to this version and resolves all version conflicts on this file.

        Using this function requires the HTTP POST method.

        Throws :class:`TeamDriveException`, if the call fails.

        .. seealso:: :any:`setCurrentVersion`

        .. versionadded:: 4.1.4

        :param versionId: The Id of the version
        :type versionId: int
        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise TeamDriveException: Failed to queue command.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._checkedCall("resolveVersionConflict", {"versionId": str(versionId)}, method="POST")

    def getAddressbookIds(self):
        """
        Returns all knows Addressbook Ids.

        :rtype: list[int]
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._call("getAddressbookIds", {})

    def getAddressbook(self, id):
        """
        Returns information about a given Addressbook Id.

        .. seealso:: :any:`getFullAddressbook` for an example

        :param id: the Address Id.
        :type id: int
        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._call("getAddressbook", {"id": str(id)})

    def getFullAddressbook(self):
        """
        Returns all Addressbook entries at once.

        **Example**:

            .. code-block:: bash

                $ python ./TeamDriveApi.py '127.0.0.1:45454' getFullAddressbook

            .. code-block:: json

                [
                  {
                    "id": 1,
                    "email": "info@teamdrive.net",
                    "name": "My Name",
                    "icon": "self"
                  }
                ]

        .. note::

            As this function was added with TeamDrive 4, the fall back for TeamDrive 3
            is to iterate over all Addressbook Ids, therefore this function is much slower.

        .. versionadded:: 4.0

        :rtype: list[dict]
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        try:
            return self._call("getFullAddressbook", {})
        except ValueError:
            return map(self.getAddressbook, self.getAddressbookIds())

    def addAddressbook(self, name):
        """
        Adds a new Address book entry. TeamDrive will lookup that name on the RegServer. If that fails, an
        exception is thrown. You cannot add email addresses.

        Using this function requires the HTTP POST method.

        :param name: the new Addressbook's name.
        :type name: str
        :return: the id of the new entry.
        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise TeamDriveException: user is unknown.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._checkedCall("addAddressbook", {"name": name}, method="POST")

    def getComments(self, fileId):
        """
        Returns a list of all comments that are attached to a specific file in a space.

        **Example** ::

            >>> api = TeamDriveApi()
            >>> api.getComments(4183)

        :returns:

            .. code-block:: json

                [
                  {
                    "creator": "test",
                    "creatorAddressId": 1,
                    "text": "file baz",
                    "created": "2015-09-01T15:15:31Z",
                    "commentId": 16,
                    "initials": "te"
                  },
                  {
                    "creator": "test",
                    "creatorAddressId": 1,
                    "deleted": true,
                    "created": "2015-09-01T15:15:28Z",
                    "commentId": 15,
                    "initials": "te"
                  }
                ]

        .. seealso:: :any:`getSpaceComments`, :any:`addComment`, :any:`addSpaceComment` and :any:`removeComment`

        .. versionadded:: 4.1.1

        :param fileId: The corresponding id of the file
        :type fileId: int
        :return: A list of comments.
        :rtype: list[dict]
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._call("getComments", {"fileId": str(fileId)})

    def getSpaceComments(self, spaceId):
        """
        Returns a list of all comments that are attached to the Space itself.

        **Example** ::

            >>> api = TeamDriveApi()
            >>> api.getSpaceComments(13)

        :returns:

            .. code-block:: json

                [
                  {
                    "creator": "test",
                    "creatorAddressId": 1,
                    "text": "file baz",
                    "created": "2015-09-01T15:15:31Z",
                    "commentId": 16,
                    "initials": "te"
                  },
                  {
                    "creator": "test",
                    "creatorAddressId": 1,
                    "deleted": true,
                    "created": "2015-09-01T15:15:28Z",
                    "commentId": 15,
                    "initials": "te"
                  }
                ]

        .. seealso:: :any:`getComments`, :any:`addComment`, :any:`addSpaceComment` and :any:`removeComment`

        .. versionadded:: 4.1.1

        :param spaceId: The Space id.
        :type spaceId: int
        :return: A list of comments.
        :rtype: list[dict]
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._call("getSpaceComments", {"spaceId": str(spaceId)})

    def addComment(self, fileId, comment, notifyMembers):
        """
        Attaches the comment to the file.

        Using this function requires the HTTP POST method.

        .. seealso:: :any:`getComments`, :any:`getSpaceComments`, :any:`addSpaceComment` and :any:`removeComment`

        .. versionadded:: 4.2.1

        :param notifyMembers: Send notification to all members (default is false)
        :type notifyMembers: bool

        .. versionadded:: 4.1.1

        :param fileId: The corresponding id of the file
        :type fileId: int
        :param comment: The content of the new comment
        :type comment: str
        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise TeamDriveException: Adding this comment failed.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._checkedCall("addComment", {"fileId": str(fileId), "comment": comment, "notifyMembers": str(notifyMembers)}, method="POST")

    def addSpaceComment(self, spaceId, comment, notifyMembers):
        """
        Adds this comment to the Space.

        Using this function requires the HTTP POST method.

        .. seealso:: :any:`getComments`, :any:`getSpaceComments`, :any:`addComment` and :any:`removeComment`

        .. versionadded:: 4.2.1

        :param notifyMembers: Send notification to all members (default is false)
        :type notifyMembers: bool

        .. versionadded:: 4.1.1

        :param spaceId: The Space id.
        :type spaceId: int
        :param comment: The content of the new comment
        :type comment: str
        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise TeamDriveException: Adding this comment failed
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._checkedCall("addSpaceComment", {"spaceId": str(spaceId), "comment": comment, "notifyMembers": str(notifyMembers)}, method="POST")

    def removeComment(self, commentId):
        """
        Marks this comment as "removed"

        Using this function requires the HTTP POST method.

        .. seealso:: :any:`getComments`, :any:`getSpaceComments`, :any:`addComment` and :any:`addSpaceComment`

        .. versionadded:: 4.1.1

        :param commentId: The id of the comment that will be deleted.
        :type commentId: int
        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise TeamDriveException: Removing this comment failed
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._checkedCall("removeComment", {"commentId": str(commentId)}, method="POST")

    def getSettings(self):
        """
        Returns a `list` of settings, with each setting a `dict`. An incomplete list of settings can be found in the :ref:`teamdrived-options` Section.

        **Example** :

            .. code-block:: json

                {
                    "key": "identifier",
                    "label": "readable label",
                    "help": "help text",
                    "type": "string|path|integer|bool|enum|bitmask",
                    "value": "current value",
                    "options": ["enum option 1", "enum option 2"],
                    "flags": [{bit: <bit flag>, label: "readable label"}, ...]
                }

        :any:`setSetting` can be used to set a specific setting.

        :rtype: list
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._call("getSettings", {})

    def setSetting(self, key, value):
        """
        Set a Setting. must be a valid key as in :any:`getSettings`. An incomplete list of settings can be found in the :ref:`teamdrived-options` Section.

        Using this function requires the HTTP POST method.

        :param key: The name of the setting
        :type key: str
        :param value: The new value of the setting
        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._checkedCall("setSetting", {"key": str(key), "value": str(value)}, method="POST")

    def setLicenseKey(self, licenseKey):
        """
        Overwrite the license key of this agent.

        Using this function requires the HTTP POST method.

        Returns True, if the license key was successfully changed otherwise raises :class:`TeamDriveException` Exception

        .. seealso:: :any:`setAccountEmail` to set the registration e-mail address. :any:`getLicense` to retrieve license infos.

        :param licenseKey: The new license key
        :type licenseKey: str
        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._checkedCall("setLicenseKey", {"licenseKey": str(licenseKey)}, method="POST")

    def setAccountEmail(self, email):
        """
        Sets the account e-mail.

        Using this function requires the HTTP POST method.

        Returns True, if the license key was successfully changed otherwise raises :class:`TeamDriveException` Exception

        .. seealso:: :any:`setLicenseKey` to set the license key.

        .. versionadded:: 4.1.1

        :param email: The new e-mail address
        :type email: str
        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise ValueError: no JSON returned. Wrong URL?
        :raise TeamDriveException: Failed to set email.
        """
        return self._checkedCall("setAccountEmail", {"email": email}, method="POST")

    def getActivities(self, spaceIds=None, fromDate=None, toDate=None, searchTerm=None, typeFilter=None, userFilter=None):
        """
        Returns a list of all activities for the given space list within the date period or 
        for the last 30 days. If :code:`hasDetails` is :code:`true`, a call
        to :any:`getActivityDetails` with the given activityId will return a list of details.

        Possible activity types are:

        .. hlist::

            * :code:`userInvited`
            * :code:`userJoined`
            * :code:`userLeft`
            * :code:`userKicked`
            * :code:`commentCreated`
            * :code:`commentDeleted`
            * :code:`spaceCommentCreated`
            * :code:`spaceCommentDeleted`
            * :code:`versionAdded`
            * :code:`versionDeleted`
            * :code:`versionPublished`
            * :code:`versionUnpublished`
            * :code:`versionExpired`

        .. versionchanged:: 4.2.0

        Added new activity types:

        .. hlist::

            * :code:`userRightsChanged`
            * :code:`userProfileUserNameChanged`
            * :code:`userProfileEmailChanged`
            * :code:`userProfilePhoneChanged`
            * :code:`userProfileMobileChanged`
            * :code:`userProfileNotesChanged`
            * :code:`userProfilePictureChanged`
            * :code:`userProfileInitialsChanged`
            * :code:`userLicenseChanged`
            * :code:`userRegEmailChanged`
            * :code:`spaceMaxVersionsOnServerChanged`
            * :code:`spaceMetaDataChanged`
            * :code:`spaceMetaDataDeleted`
            * :code:`spaceDeletedOnServer`
            * :code:`serverAccessSent`
            * :code:`serverAccessFailedToSend`
            * :code:`serverAccessReceived`
            * :code:`serverAccessDeleted`
            * :code:`invitationSent`
            * :code:`invitationFailed`
            * :code:`invitationDeclined`
            * :code:`invitationReceived`
            * :code:`fileAdded`
            * :code:`fileRenamed`
            * :code:`fileMoved`
            * :code:`fileTrashed`
            * :code:`fileUntrashed`
            * :code:`fileDeleted`
            * :code:`fileChanged`
            * :code:`fileCurrentVersionChanged`
            * :code:`folderAdded`
            * :code:`folderRenamed`
            * :code:`folderMoved`
            * :code:`folderTrashed`
            * :code:`folderUntrashed`
            * :code:`folderDeleted`
            * :code:`folderChanged`
            * :code:`fileNameConflict`
            * :code:`fileNameConflictResolved`
            * :code:`filePathInvalid`
            * :code:`filePathInvalidResolved`
            * :code:`versionPublishMetaChanged`
            * :code:`versionConflict`
            * :code:`versionConflictResolved`
            * :code:`versionExported`
            * :code:`emailNotificationSent`
            * :code:`emailNotificationFailedToSend`
            * :code:`workingDirFull`
            * :code:`runningOutOfDiskSpace`
            * :code:`noDiskFreeSpace`
            * :code:`automaticTrashCleanupChanged`
            * :code:`temporaryError`

        .. seealso:: :any:`getActivityDetails`

        .. versionadded:: 4.1.1

        :return: A list of activities.
        :rtype: list[dict]
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise ValueError: no JSON returned. Wrong URL?
        
        .. versionchanged:: 4.2.0

        :param spaceIds: A list of space IDs. If empty, all Spaces will be used.
        :type spaceIds: str
        :param fromDate: ISO 8601 extended format: either YYYY-MM-DD for dates or YYYY-MM-DDTHH:mm:ss, YYYY-MM-DDTHH:mm:ssTZD (e.g., 1997-07-16T19:20:30+01:00) for combined dates and times. If empty the last 30 days will be returned.
        :type fromDate: str
        :param toDate: ISO 8601 extended format: either YYYY-MM-DD for dates or YYYY-MM-DDTHH:mm:ss, YYYY-MM-DDTHH:mm:ssTZD (e.g., 1997-07-16T19:20:30+01:00) for combined dates and times.
        :type toDate: str

        .. versionadded:: 4.3.2

        :param searchTerm: Search for activities that match the given string
        :type searchTerm: str
        :param typeFilter: Comma-separated combination of: :code:`conflictAndError`, :code:`invitationAndMember`, :code:`fileAndVersion`,
            :code:`comment`, :code:`publish`, :code:`all`
        :type typeFilter: str
        :param userFilter: one of: :code:`myUser`, :code:`otherUsers`, :code:`all`
        :type userFilter: str

        """
        params = {}
        if spaceIds:
            params["spaceIds"] = spaceIds
        if fromDate:
            params["fromDate"] = fromDate
        if toDate:
            params["toDate"] = toDate
        if searchTerm:
            params["searchTerm"] = searchTerm
        if typeFilter:
            params["typeFilter"] = typeFilter
        if userFilter:
            params["userFilter"] = userFilter
        return self._call("getActivities", params, method="POST")

    def getActivityDetails(self, activityId):
        """
        Returns a list of all activity details for an activity.

        .. seealso:: :any:`getActivities`

        .. versionadded:: 4.1.1

        :param activityId: The activityId as returned by :any:`getActivities`
        :type activityId: str
        :return: A list of activityDetails.
        :rtype: list[dict]
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._call("getActivityDetails", {"activityId": str(activityId)})

    def getActivityMetas(self, activityId):
        """
        Returns a list of all activity meta information for an activity.

        .. seealso:: :any:`getActivities`

        .. versionadded:: 4.2.1

        :param activityId: The activityId as returned by :any:`getActivities`
        :type activityId: str
        :return: A list of activityMeta.
        :rtype: list[dict]
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._call("getActivityMetas", {"activityId": str(activityId)})

    def getProfilePicture(self, id):
        """
        Returns the profile picture of the address.

        .. seealso:: :any:`setProfilePicture` to set the profile picture of the current user.

        .. versionadded:: 4.1.1

        :param id: The addressId.
        :type id: int
        :return: The picture.
        :rtype: bytes | dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        """
        return self._call("getProfilePicture", {"id": str(id)}, expectBinary=True)

    def setProfilePicture(self, data):
        """
        Sets the profile picture of the current user.

        .. seealso:: :any:`getProfilePicture` to retrieve profile pictures and :any:`clearProfilePicture` to clear the picture.

        .. versionadded:: 4.1.1

        :param data: The blob.
        :type data: bytes
        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._call("setProfilePicture", data, method="POST")

    def clearProfilePicture(self):
        """
        Clears the profile picture of the current user.

        .. seealso:: :any:`getProfilePicture` to retrieve profile pictures and :any:`setProfilePicture`

        .. versionadded:: 4.1.1

        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._call("clearProfilePicture", method="POST")

    def setProfileInfo(self, email=None, phone=None, mobile=None):
        """
        Sets profile information of the current user.

        .. seealso:: :any:`setProfilePicture` to set the profile picture.

        .. versionadded:: 4.1.1

        :param email: Optional, the new e-mail.
        :type email: str | None
        :param phone: Optional, the new phone number.
        :type phone: str | None
        :param mobile: Optional, the new mobile number.
        :type mobile: str | None
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        params = {}
        if email:
            params["email"] = email
        if phone:
            params["phone"] = phone
        if mobile:
            params["mobile"] = mobile
        return self._call("setProfileInfo", params, method="POST")

    def getSpaceSettings(self, spaceId):
        """
        Get space-specific settings.

        .. versionadded:: 4.1.2

        :param spaceId: The Space id.
        :type spaceId: int
        :rtype: dict
        """
        return self._checkedCall("getSpaceSettings", {"id": str(spaceId)}, method="GET")

    def setSpaceSetting(self, spaceId, key, value):
        """
        set a Space Setting. must be a valid key as in :any:`getSpaceSettings`.

        Using this function requires the HTTP POST method.

        .. versionadded:: 4.1.2

        :param spaceId: The Space id.
        :type spaceId: int
        :param key: The name of the setting
        :type key: str
        :param value: The new value of the setting
        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        return self._checkedCall("setSpaceSetting", {"spaceId": str(spaceId), "key": str(key), "value": str(value)}, method="POST")

    def createBackup(self):
        """
        Create a backup in the local file system. To create a backup and download it, send a GET request to '/backup'. To import a TeamDrive
        backup file ('.tdbk'), send it to '/backup' via a PUT request.

        :returns: A dict where the key "path" points to the file in the local filesystem.

        .. versionadded:: 4.3.1

        :rtype: dict
        """
        return self._call("getFiles", {"spaceId": str(spaceId), "filePath": filePath, "trashed": str(trashed)})

    def importSpaceKeys(self):
        """
        Import Space keys from the server (ie. Spaces that have been deleted locally will re-appear as inactive Spaces, if the key-repository backup was enabled)

        :returns: A dict where the key "spacesImported" contains the number of imported Spaces.

        .. versionadded:: 4.3.1

        :rtype: dict
        """
        return self._call("importSpaceKeys", method="POST")

    def createSnapshot(self, spaceId):
        """
        Create a snapshot that can later be used to restore the Space (see :any:`commitCurrentSnapshot`)

        Using this function requires the HTTP POST method.

        .. versionadded:: 4.3.2

        :param spaceId: The Space id.
        :type spaceId: int
        :rtype: bool
        """
        return self._checkedCall("createSnapshot", {"spaceId": str(spaceId)}, method="POST")

    def getSnapshots(self, spaceId):
        """
        Get snapshots that can later be used to restore the Space (see :any:`commitCurrentSnapshot`)

        .. versionadded:: 4.3.2

        :rtype: list[dict]
        """
        return self._call("getSnapshots", {"spaceId": str(spaceId)})

    def commitCurrentSnapshot(self, spaceId):
        """
        Commit the snapshot that is currently being previewed.

        The process for restoring a Space to a snapshot is as follows:

        1. 'preview' a snapshot by calling the :any:`joinSpace` function with the 'snapshotId' parameter

            * The Space is reverted *locally* to the state stored in the snapshot, in read-only mode
            * The user can then decide whether they want to commit this change.

        2. 'commit' the change by calling :any:`commitCurrentSnapshot`

            * The Space is reverted on the server (this affects not only files but also the members in the Space, member permissions, etc.)

        While in preview-mode, you can also call :any:`joinSpace` again with a different snapshot ID to preview another snapshot, or
        without a snapshot ID to cancel the rollback and restore the Space to its usual state.

        Using this function requires the HTTP POST method.

        .. versionadded:: 4.3.2

        :param spaceId: The Space id.
        :type spaceId: int
        :rtype: bool
        """
        return self._checkedCall("commitCurrentSnapshot", {"spaceId": str(spaceId)}, method="POST")

    def deleteSnapshot(self, spaceId, snapshotId):
        """
        Delete snapshots for this Space based on the given ``snapshotId``

        .. warning::
            This function deletes *all* snapshots up to and including the one specified by ``snapshotId``

        Using this function requires the HTTP POST method.

        .. versionadded:: 4.5.0

        :param spaceId: The Space id.
        :type spaceId: int
        :param snapshotId: The snapshot id.
        :type snapshotId: int
        :rtype: bool
        """
        return self._checkedCall("deleteSnapshot", {"spaceId": str(spaceId), "snapshotId": str(snapshotId)}, method="POST")

    def setSpaceSyncEnabled(self, spaceId, enabled):
        """
        Enable/disable synchronization for a Space

        Using this function requires the HTTP POST method.

        .. versionadded:: 4.5.0

        :param spaceId: The Space id.
        :type spaceId: int
        :param enabled: Whether to enable synchronization
        :type enabled: bool
        :rtype: dict
        """
        return self._checkedCall("setSpaceSyncEnabled", {"id": str(spaceId), "enabled": str(enabled)}, method="POST")

    def setSpacePath(self, spaceId, localPath):
        """
        Set the root path for a Space in the local file system. This can be used to move a Space to a new location, or
        to resolve a "Space root not found error" (eg. when a local folder name has changed and TeamDrive can no longer
        find the Space).

        .. note:: Because this function affects the file system, it can only be called from the host that the API is running on (calling it from elsewhere will result in an "Unknown function" error).

        Using this function requires the HTTP POST method.

        .. versionadded:: 4.5.0

        :param spaceId: The Space id.
        :type spaceId: int
        :param localPath: New root folder for the Space
        :type localPath: str
        :rtype: dict
        """
        return self._checkedCall("setSpaceSyncPath", {"spaceId": str(spaceId), "localPath": localPath}, method="POST")

    def emptyTrash(self, spaceId):
        """
        Permanently delete all trashed files for the given Space.

        .. note:: Depending on the number of files / file versions, the contents of the trash may not immediately disappear after this function returns

        Using this function requires the HTTP POST method.

        .. versionadded:: 4.5.3

        :param spaceId: The Space id.
        :type spaceId: int
        :rtype: dict
        """
        return self._checkedCall("emptyTrash", {"spaceId": str(spaceId)}, method="POST")

    def sse(self):
        """
        This is the Server Side Events api listening on `/sse`. This API sends events according to the Server Side Events specification.

        The implementation of the python wrapper requires Python 3.

        Example Usage::

            $ python3 TeamDriveApi.py '127.0.0.1:45454' sse
            data: { "event" : "FileEntryChanged", "fileId" : 0, "hasConflict" : 0, "isDir" :
                    false, "isTrashed" : false, "name" : "my file", "path" :
                    "/home/teamdrive/Spaces/My Space", "spaceId" : 3, "synced" : true }

            data: { "event" : "SpaceModified", "spaceId" : 3 }

        Possible Event types are: `FileEntryChanged`, `VersionEntryChanged`, `FileVersionPublished`, `FileVersionUnpublished` and `SpaceModified`

        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise ValueError: no JSON returned. Wrong URL?
        """
        import io
        headers = self._getAuthorizationHeader()
        self._h.request("GET", "/sse", headers=headers)
        r = self._h.getresponse()
        hio = io.TextIOWrapper(io.BufferedReader(r, 1))
        hio._CHUNK_SIZE = 1
        for l in hio:
            print(l.strip())
            sys.stdout.flush()

    def sendSupportRequest(self, contactName, contactEmail, contactTel, description, sendLogs):
        """
        Uploads a support request to the server (see the ``Client Log Files`` chapter in the Registration server
        documentation on how to configure log uploads)

        .. versionadded:: 4.5.0

        :param contactName:
        :type contactName: str | None
        :param contactEmail:
        :type contactEmail: str | None
        :param contactTel:
        :type contactTel: str | None
        :param description: Description of the problem
        :type description: str | None
        :param sendLogs: Whether or not to attach log files (true if not specified)
        :type sendLogs: bool | None
        :rtype: dict
        """
        params = {
            "contactName": contactName,
            "contactEmail": contactEmail,
            "contactTel": contactTel,
            "description": description,
            "sendLogs": sendLogs
        }

        return self._checkedCall("sendSupportRequest", params, method="POST")

    def setFeatureChoice(self, featureId, useNewFeature, spaceId = 0):
        """
        Set whether the given feature should be used in this Space (ie. suppresses further ``Error_Feature_Upgrade`` responses for this Space and feature)
        
        .. seealso:: Chapter :any:`Error_Feature_Upgrade`
        
        Note that a response with ``useNewFeature == False`` will only suppress further error responses for features that have an older behaviour to fall back to.
        
        .. versionadded:: 4.6.3
        
        :param featureId: The id of the feature
        :type featureId: int
        :param useNewFeature: Whether to use the new feature
        :type useNewFeature: bool
        :param spaceId: (Optional, depending on the feature) The Space that this choice is being made for
        :type spaceId: int | None
        :rtype: dict
        """
        
        return self._checkedCall("setFeatureChoice", self._mkParams(locals()), method="POST")

    def unsetFeatureChoice(self, featureId, spaceId):
        """
        Cause ``Error_Feature_Upgrade`` responses for this Space and feature to be shown even if they were suppressed by a previous ``setFeatureChoice`` call.
        
        .. seealso:: Chapter :any:`Error_Feature_Upgrade`
        
        .. versionadded:: 4.6.3
        
        :param featureId: The id of the feature
        :type featureId: int
        :param spaceId: (Optional, depending on the feature) The Space that this choice is being made for
        :type spaceId: int | None
        :rtype: dict
        """
        
        return self._checkedCall("unsetFeatureChoice", self._mkParams(locals()), method="POST")

    def setAutoDelete(self, fileId, autoDelete):
        """
        In 'Data Retention' Spaces (see :ref:`Data Retention <DataRetention>`) with the "Auto Trash" option enabled, files are automatically moved to the trash after their
        retention period expires. This can be disabled per-file by setting ``autoDelete`` to ``False`` with this call.
        
        .. versionadded:: 4.6.3
        
        :param fileId: The id of the file
        :type fileId: int
        :param autoDelete: Whether to auto-trash the file
        :type autoDelete: bool
        :rtype: dict
        """
        
        return self._checkedCall("setAutoDelete", self._mkParams(locals()), method="POST")

    def setRetentionStartTime(self, fileId, startTime):
        """
        In 'Data Retention' Spaces (see :ref:`Data Retention <DataRetention>`), files are supposed to be un-deletable for a given minimum period of time (the "retention period"). In some cases,
        however, the actual legal retention period for a file may already have begun before the Space was created -> in such case, this API call can be used to indicate the date
        at which the retention period for this file actually started.
        
        .. versionadded:: 4.6.3
        
        :param fileId: The id of the file
        :type fileId: int
        :param startTime: Date from which the retention period for the file should begin (YYYY-MM-DD)
        :type startTime: str
        :rtype: dict
        """
        
        return self._checkedCall("setRetentionStartTime", self._mkParams(locals()), method="POST")

class TeamDriveApi (InternalTeamDriveApi):
    __doc__ = InternalTeamDriveApi.__init__.__doc__

    def getSpaceByName(self, spaceName):
        """
        This is convenience wrapper of the Python module that is not part of the official API.

        Returns a Space by a given Space name.
        Throws a :class:`TeamDriveException`, if there is no Space with this name.

        :param spaceName: The Name.
        :type spaceName: str | unicode
        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise TeamDriveCallFailed: There is no Space with this name
        :raise ValueError: no JSON returned. Wrong URL?
        """
        try:
            return getFirst(filter(lambda s: (s["name"] == spaceName) and (s["status"] == "active"), self.getSpaces()))
        except StopIteration:  # raised by python 2
            raise TeamDriveCallFailed("no such Space:" + spaceName, None)
        except IndexError:  # raised by python 3
            raise TeamDriveCallFailed("no such Space:" + spaceName, None)

    def getAddressbookByName(self, addressName):
        """
        This is convenience wrapper of the Python module that is not part of the official API.

        Returns an Addressbook entry by a given user name.
        Throws a TeamDriveException, if there is no Space with this name.

        :param addressName: The Name.
        :type addressName: str
        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        :raise socket.error: a Connection Error.
        :raise ConnectionRefusedError: a Connection Error.
        :raise TeamDriveCallFailed: There is no Space with this name
        :raise ValueError: no JSON returned. Wrong URL?
        """
        try:
            return getFirst(filter(lambda s: s["name"] == addressName, self.getFullAddressbook()))
        except StopIteration:  # raised by python 2
            raise TeamDriveCallFailed("no such Addressbook:" + addressName, None)
        except IndexError:  # raised by python 3
            raise TeamDriveCallFailed("no such Addressbook entry:" + addressName, None)

    def putFileContent(self, spaceId, spacePath, filePath):
        """
        .. note:: This is not part of the HTTP API. See :any:`putFile` instead.

        **Example**:

            .. code-block:: bash

                $ python ./TeamDriveApi.py '127.0.0.1:45454' putFileContent 3 /testfile ./testfile

        :param spaceId: The Id of the Space
        :type spaceId: int
        :param spacePath: the path of the file in the Space.
        :type spacePath: str
        :param filePath: the path to the local file
        :type filePath: str
        :rtype: dict
        :raise httplib.HTTPException: a Connection Error.
        """
        with open(filePath, mode='rb') as f:
            return self.putFile(spaceId, spacePath, f.read())


def getArgs(usage):
    try:
        from docopt import docopt
    except ImportError:
        def docopt(usage, version):
            if len(sys.argv) < 6:
                print("Usage: %s <server-url> --user USER --pass PASS <command> args...\ninstall docopt to get a much better usage. Try executing 'sudo pip install docopt'" % sys.argv[0])
                exit(1)
            arguments = {
                "--info": False,
                "--user": sys.argv[3],
                "--pass": sys.argv[5],
                "<server-url>": sys.argv[1],
                sys.argv[6]: True
            }
            return arguments

    return docopt(usage, version='TeamDrive 4')


def main():
    import inspect

    inspectApi = TeamDriveApi("")
    commands = list(filter(lambda x: not x.startswith("_"), dir(inspectApi)))

    usage = """TeamDriveApi.

Usage:"""
    for command in commands:
        arglist = " ".join(["<" + a + ">" for a in inspect.getargspec(getattr(TeamDriveApi, command)).args[1:]])
        usage += """
  TeamDriveApi.py (<server-url> [options] | --info) """ + command + " " + arglist
    usage += """
  TeamDriveApi.py <command> --info"""
    usage += """
  TeamDriveApi.py (-h | --help)
  TeamDriveApi.py --version


Options:
  --user USER   Set the user name
  --pass PASS   Set the password
  --info        Show info about this command
  -h --help     Show this screen.
  --version     Show version.
"""
    arguments = getArgs(usage)

    if arguments["--info"]:
        print(help(getattr(TeamDriveApi, arguments["<command>"])))
    elif arguments["<server-url>"]:
        command = list(filter(lambda a: a[0] in commands and a[1] == True, arguments.items()))[0][0]

        skip = 3
        if arguments["--user"]: skip += 2
        if arguments["--pass"]: skip += 2

        params = sys.argv[skip:]

        api = TeamDriveApi(arguments["<server-url>"], arguments["--user"], arguments["--pass"])
        result = (getattr(TeamDriveApi, command)(api, *params))
        if isinstance(result, dict) or isinstance(result, list):
            print(json.dumps(result, indent=2))
        else:
            print(result)

if __name__ == "__main__":
    main()

if 'epydoc' in sys.modules:
    __all__ = ["TeamDriveException", "TeamDriveApi", "InternalTeamDriveApi"]
else:
    __all__ = ["TeamDriveException", "TeamDriveApi"]
